def git_repo = 'http://gogs-cimb-infra.apps.cimb.rht-labs.com/bkawiswara/nodejs-apps-template.git'
def public_route_prefix = ''
def git_branch = 'master'
def nexus_base_url = 'http://nexus-cimb-nexus.apps.cimb1.rht-labs.com'
def nexus_deploy_repo = "${nexus_base_url}/repository/cimb-niaga-static-apps"
def ocp_project = 'cimb-apps'

def appName
def appFullVersion
def gitCommitId

node ('nodejs') {
   stage ('Checkout The Code'){
      git url: "${git_repo}", branch: "${git_branch}"
   }

   stage ('Prepare'){
      withCredentials([[$class: 'UsernamePasswordMultiBinding', 
         credentialsId: 'bfsIntNexusJenkinsCredential',
         usernameVariable: 'nexus_username', passwordVariable: 'nexus_password']]) {
               sh """
                  echo 'Downloading ci-cd templates...'
                  curl --fail -u ${nexus_username}:${nexus_password} -o cicd-template.tar.gz ${nexus_base_url}/repository/cicd-general/cicd-template.tar.gz
                  mkdir cicd-template && tar -xzvf ./cicd-template.tar.gz -C "\$(pwd)/cicd-template"
                  """
      }

      appName = sh( script: 'node -e "console.log(require(\'./package.json\').name);"', returnStdout: true).trim()
      appFullVersion = sh( script: 'node -e "console.log(require(\'./package.json\').version);"', returnStdout: true).trim()
      appFullVersion = appFullVersion.substring(0, appFullVersion.lastIndexOf('.')) + ".${BUILD_NUMBER}"
      gitCommitId = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
   }

    stage ('Install Dependencies'){
        sh 'npm install'
    }
    
    stage ('Build Package'){
       sh 'npm run build'
    }

    stage ('Test'){
       sh 'npm run test-ci'
    }

    stage ('Package'){
       zip zipFile: "${appName}-${appFullVersion}.zip", dir: 'build'
    }

    stage("Archive") {
       withCredentials([[$class: 'UsernamePasswordMultiBinding',
           credentialsId: 'bfsIntNexusJenkinsCredential',
           usernameVariable: 'nexus_username', passwordVariable: 'nexus_password']]) {
               sh "curl -u ${nexus_username}:${nexus_password} --upload-file ./${appName}-${appFullVersion}.zip ${nexus_deploy_repo}/${appName}/${appName}-${appFullVersion}.zip"
       }
    }
   stage ('OpenShift Build'){
      appMajorVersion = appFullVersion.substring(0, appFullVersion.indexOf('.'))

        sh """
                set -x
                set -e

                oc project ${ocp_project}
                oc process -f ./cicd-template/openshift/build-config-template.s2i.yaml -n ${ocp_project} \
                  -p S2I_BUILD_IMAGE='nginx-114-rhel7' -p S2I_BUILD_IMAGE_PULL_SECRET='12388571-jijiechen-pull-secret' \
                  -p APP_NAME='${appName}' -p APP_FULL_VERSION='${appFullVersion}' -p APP_MAJOR_VERSION='${appMajorVersion}' \
                  -p GIT_COMMIT_ID=${gitCommitId} -p JENKINS_BUILD_NUMBER=${BUILD_NUMBER} \
                  | oc apply -n ${ocp_project} -f -
                
                oc start-build ${appName}-v${appMajorVersion} -n ${ocp_project} --from-dir ./build --follow
           """
    }

    stage ('OpenShift Applciation ConfigMap'){
        sh """
                set -x
                set -e
                export APP_CONFIG_DATA='key=value'

                oc project ${ocp_project}
                oc process -f ./cicd-template/openshift/configmap-template.yaml -n ${ocp_project} \
                  -p APP_NAME='${appName}' -p APP_FULL_VERSION='${appFullVersion}' -p APP_MAJOR_VERSION='${appMajorVersion}' \
                  -p GIT_COMMIT_ID=${gitCommitId} -p JENKINS_BUILD_NUMBER=${BUILD_NUMBER} -p CONFIG_DATA="\$APP_CONFIG_DATA" \
                  | oc apply -n ${ocp_project} -f -
           """
    }

    stage ('OpenShift Deployment'){
        sh """
            set -x
            set -e

            oc project ${ocp_project}
            oc process -f ./cicd-template/openshift/deployment-config-template.yaml -n ${ocp_project} \
                -p APP_NAME=${appName} -p APP_FULL_VERSION=${appFullVersion} -p APP_MAJOR_VERSION=${appMajorVersion}  \
                -p GIT_COMMIT_ID=${gitCommitId} -p JENKINS_BUILD_NUMBER=${BUILD_NUMBER} \
                | oc apply -n ${ocp_project} --force=true -f -
            sleep 5
            """

      if (public_route_prefix != null && public_route_prefix != ''){
        sh """
            set -x
            set -e

            oc project ${ocp_project}
            oc process -f ./cicd-template/openshift/route-template.yaml -n ${ocp_project} \
                -p APP_NAME=${appName} -p APP_FULL_VERSION=${appFullVersion} -p APP_MAJOR_VERSION=${appMajorVersion}  \
                -p GIT_COMMIT_ID=${gitCommitId} -p PUBLIC_ROUTE_PREFIX=${public_route_prefix} -p JENKINS_BUILD_NUMBER=${BUILD_NUMBER} \
                -p APP_ROUTE_PATH="/" \
                | oc apply -n ${ocp_project} --force=true -f -
            sleep 5
            """
      }
    }
}