def registry='repo-url'
def imageRepo='image-repo'
def imageName='image-name'
def credentials='id-credential-on-jenkins'
node('master') {
    def build

    stage('Clone repository') {
        /* Let's make sure we have the repository cloned to our workspace */
        checkout scm
    }

    stage('Build image') {
        /* This builds the actual image; synonymous to
         * docker build on the command line */

        build = docker.build("${registry}/${imageRepo}/${imageName}")
    }

    stage('Test image') {
        /* Ideally, we would run a test framework against our image.
         * For this example, we're using a Volkswagen-type approach ;-) */

        build.inside {
            sh 'echo "Tests passed"'
        }
    }

    stage('Push image') {
        /* Finally, we'll push the image with two tags:
         * First, the incremental build number from Jenkins
         * Second, the 'latest' tag.
         * Pushing multiple tags is cheap, as all the layers are reused. */
        docker.withRegistry("https://${registry}", "${credentials}") {
            build.push("${env.BUILD_NUMBER}")
            build.push("latest")
        }
    }
    stage('Cleaning Image'){
        sh """
            set -e
            set -x
            docker rmi ${registry}/${imageRepo}/${imageName}:${env.BUILD_NUMBER}
            docker rmi ${registry}/${imageRepo}/${imageName}:latest
        """
    }
}
