CI CD General Templates
=======


This repository maintains templates files used by CI/CD projects. 

### Usage


Pleaser refer to these documents to create CI/CD pipelines for your applications/services:

* [Create a CI/CD Pipeline For Your Maven based Microservice](https://rhlabs.atlassian.net/wiki/spaces/CIMB/pages/789905423/Create+a+CI+CD+Pipeline+For+Your+Maven+based+Microservice)
* [Create a CI/CD pipeline for your node.js static website microservice](https://rhlabs.atlassian.net/wiki/spaces/CIMB/pages/789709100/Create+a+CI+CD+pipeline+for+your+node.js+static+website+microservice)


### Files

```sh
├── Dockerfiles                         # Folders containing files for building containers
│   ├── java.Dockerfile
│   └── nodejs.Dockerfile
├── Jenkinsfile                         # Pipeline script for this project
├── Jenkinsfiles                        # Folders containing pipeline scripts for apps/svcs
│   ├── java.Jenkinsfile
|   ├── nodejs.Jenkinsfile
│   └── nginx-example
│       ├── default.conf
│       ├── readme.md
│       └── secure.key
│    
├── README.md                           # This file
├── maven                               # Files will be used during maven steps when executing CI/CD pipeline
│   ├── pom-distribution-management.xml
│   └── settings.xml
└── openshift                           # Folders containing all the OpenShift object configration templates
    ├── build-config-template.yaml
    ├── configmap-template.yaml
    ├── deployment-config-template.yaml
    └── route-template.yaml
```

